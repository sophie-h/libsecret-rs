#[macro_use]
extern crate glib;

mod auto;

pub use auto::functions::*;
pub use auto::*;
