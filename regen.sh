#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail


CRATE_API=libsecret
CRATE_SYS=libsecret-sys

CONFIG_API=${CRATE_API}/Gir.toml
CONFIG_SYS=${CRATE_SYS}/gir-secret.toml

GIR=./gir/target/release/gir

# Build the gir tool
git submodule update --init
(cd gir && cargo build --release)

# Regenerate the sys crate
rm -rf ${CRATE_SYS}/{build.rs, src/auto}
${GIR} -c ${CONFIG_SYS} -d gir-files -o ${CRATE_SYS}

# Regenerate the api crate
rm -rf ${CRATE_API}/src/auto
${GIR} -c ${CONFIG_API} -d gir-files -o ${CRATE_API}

# Run cargo fmt --all
cargo fmt --all

